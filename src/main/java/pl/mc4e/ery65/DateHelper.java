package pl.mc4e.ery65;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {
    
    public static boolean isOpen(String fromTime, String toTime){
        Date from = getTime(fromTime, false);
        Date to = getTime(toTime, false);
        Date now = Calendar.getInstance().getTime();
        if (now.before(from) && to.before(from) && now.before(to)){
            return true;
        } else if (from.before(now) && now.before(to)){
            return true;
        } else 
            return false;
        /**first point *//*if (from.getDay()>now.getDay()&&now.getDay()<to.getDay()){
            if (to.getDay()!=from.getDay()){
                if (now.before(from)&&now.before(to)&to.before(from)){
                    return true;
                } else {
                    return false;
                }
            } else 
                return false;
        *//**second point*//*} else if (from.getDay()==now.getDay()&&now.getDay()<to.getDay()) {
            if (from.getHours()<now.getHours()){
                return true;
            } else if (from.getHours()==now.getHours()){
                if (from.getMinutes()<now.getMinutes()){
                    return true;
                } else if (from.getMinutes()==now.getMinutes()){
                    if (from.getSeconds()<=now.getSeconds())
                        return true;
                    else
                        return false;
                } else {
                    return false;
                }
            } else
                return false;
        } else if (from.getDay()==now.getDay()&&now.getDay()==to.getDay()) {
            if (from.getHours()<to.getHours()&&from.getHours()<now.getHours()&&now.getHours()<to.getHours()){
                return true;
            } else if (from.getHours()<now.getHours()&& now.getHours()==to.getHours()){
                if (now.getMinutes()<to.getMinutes())
                    return true;
                else if (now.getMinutes()==to.getMinutes()){
                    if (now.getSeconds()<to.getSeconds())
                        return true;
                    else
                        return false;
                } else
                    return false;
            } else if (from.getHours()==now.getHours()&&now.getHours()<to.getHours()){
                if (from.getMinutes()<now.getMinutes()){
                    return true;
                } else if (from.getMinutes()==now.getMinutes()){
                    if (from.getSeconds()<=now.getSeconds()){
                        return true;
                    } else 
                        return false;
                } else 
                    return false;
            } else if (from.getHours()==now.getHours()&&now.getHours()==to.getHours()){
                if (from.getMinutes()<now.getMinutes()&&now.getMinutes()<to.getMinutes())
                    return true;
                else if (from.getMinutes()<now.getMinutes()&&to.getMinutes()==now.getMinutes()){
                    if (now.getSeconds()<to.getSeconds())
                        return true;
                    else
                        return false;
                } else if (now.getMinutes()==to.getMinutes()&&now.getMinutes()==from.getMinutes()){
                    if (from.getSeconds()<=now.getSeconds()&&now.getSeconds()<to.getSeconds()){
                        return true;
                    } else 
                        return false;
                } else
                    return false;
            } else if (from.getHours()<now.getHours()&&now.getHours()>to.getHours()){
                return true;
            } else if (from.getHours()==now.getHours()&&now.getHours()>to.getHours()){
                if (from.getMinutes()<now.getMinutes())
                    return true;
                else if (from.getMinutes()==now.getMinutes()){
                    if (from.getSeconds()<=now.getSeconds())
                        return true;
                    else
                        return false;
                } else 
                    return false;
            } else 
                return false;
        } else
            return false;*/
    }
    
    public static Date getTime(String time, boolean nextTime){
        Calendar cal = Calendar.getInstance();
        if (getTime(time, TimeValue.DAYS)==-2){
            cal.set(Calendar.DAY_OF_WEEK, getDay(Calendar.getInstance().get(Calendar.DAY_OF_WEEK), nextTime));
            cal.set(Calendar.HOUR_OF_DAY, getTime(time, TimeValue.HOURS));
            cal.set(Calendar.MINUTE, getTime(time, TimeValue.MINUTES));
            cal.set(Calendar.SECOND, getTime(time, TimeValue.SECONDS));
        } else {
            cal.set(Calendar.DAY_OF_WEEK, getTime(time, TimeValue.DAYS));
            cal.set(Calendar.HOUR_OF_DAY, getTime(time,TimeValue.HOURS));
            cal.set(Calendar.MINUTE, getTime(time, TimeValue.MINUTES));
            cal.set(Calendar.SECOND, getTime(time, TimeValue.SECONDS));
        }
        return cal.getTime();
    }
    
    private static int getDay(int today, boolean nextTime){
        if (!nextTime)
            return today;
        else {
            if (today + 1 < 7)
                return today + 1;
            else
                return 1;
        }
    }
    
    public static int getTime(String time, TimeValue value){
        //example: mon,22:00:00
        String[] a = time.split(",");
        int b = 0;
        try {
            if (value == TimeValue.MINUTES){
                b = Integer.valueOf(a[1].split(":")[1]);
            } else if (value == TimeValue.HOURS){
                b = Integer.valueOf(a[1].split(":")[0]);
            } else if (value == TimeValue.SECONDS){
                b = Integer.valueOf(a[1].split(":")[2]);
            } else if (value == TimeValue.DAYS){
                String day = a[0];
                if (day.startsWith("pon")||day.startsWith("mon")){
                    b = Calendar.MONDAY; 
                } else if (day.startsWith("wt")||day.startsWith("tu")){
                    b = Calendar.TUESDAY;
                } else if (day.startsWith("sr")||day.startsWith("wed")){
                    b = Calendar.WEDNESDAY;
                } else if (day.startsWith("cz") || day.startsWith("th")){
                    b = Calendar.THURSDAY;
                } else if (day.startsWith("pi")||day.startsWith("fr")){
                    b = Calendar.FRIDAY;
                } else if (day.startsWith("sob")||day.startsWith("sat")){
                    b = Calendar.SATURDAY;
                } else if (day.startsWith("nie")||day.startsWith("sun")){
                    b = Calendar.SUNDAY;
                } else {
                    b = -2;
                }
            }
        } catch (NumberFormatException e){
            b = 0;
        }
        return b;
    }
    
    
    public enum TimeValue {
        DAYS,MINUTES,SECONDS,HOURS;
    }

}
