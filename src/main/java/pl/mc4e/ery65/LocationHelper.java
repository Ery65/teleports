package pl.mc4e.ery65;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Chunk;
import org.bukkit.Location;

import com.google.common.collect.Lists;

public class LocationHelper {
    
    public static List<Integer> getIntegerList(int value, int svalue){
        Set<Integer> a = new HashSet<Integer>();
        List<Integer> fixed = Lists.newArrayList();
        if (value<=svalue){
            for (int i=value;i<=svalue;i++){
                a.add(i);
            }
            for (int i :a){
                fixed.add(i);
            }
        } else {
            for (int i = svalue;i<=value;i++){
                a.add(i);
            }
            for (int i : a){
                fixed.add(i);
            }
        }
        return fixed;
    }
    
    public static List<Chunk> getCunks(Location loc1, Location loc2){
        Set<Chunk> aa = new HashSet<Chunk>();
        List<Chunk> a = Lists.newArrayList();
        if (loc1.distance(loc2)<=16){
            aa.add(loc1.getChunk());
            aa.add(loc2.getChunk());
            for(Chunk s: aa){
                a.add(s);
            }
        } else {
            for (int x = Math.min(loc1.getBlockX(), loc2.getBlockX());x<=
                    Math.max(loc1.getBlockX(), loc2.getBlockX());x++){
                for (int y = Math.min(loc1.getBlockY(), loc2.getBlockY()); y<=
                        Math.max(loc1.getBlockY(), loc2.getBlockY());y++){
                    for (int z = Math.min(loc1.getBlockZ(), loc2.getBlockZ()); z<=
                            Math.max(loc1.getBlockZ(), loc2.getBlockZ());z++){
                        Location loc = new Location(loc1.getWorld(),x,y,z);
                        aa.add(loc.getChunk());
                        z+=15;
                    }
                    y+=15;
                    
                }
                x+=15;
                
            }
        }
        for (Chunk s: aa){
            a.add(s);
        }
        return a;
    }
    
    public static Map<String, Object> getLocation(Location loc){
        Map<String, Object> o = new HashMap<String, Object>();
        o.put("x", loc.getBlockX());
        o.put("y", loc.getBlockY());
        o.put("z", loc.getBlockZ());
        return o;
    }
    
    public static Map<String, Object> getLocationW(Location loc){
        Map<String, Object> o = new HashMap<String, Object>();
        o.put("world", loc.getWorld().getName());
        o.put("x", loc.getBlockX());
        o.put("y", loc.getBlockY());
        o.put("z", loc.getBlockZ());
        o.put("yaw", loc.getYaw());
        o.put("pitch", loc.getPitch());
        return o;
    }

}
