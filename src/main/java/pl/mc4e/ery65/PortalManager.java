package pl.mc4e.ery65;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class PortalManager {
    
    public static void openPortal(String time, String portal, boolean nextTime){
        Timer tim = new Timer();
        Date times = DateHelper.getTime(time, nextTime);
        tim.schedule(new OpenPortal(portal, times, tim), times);
        Teleports.getInstance().addTimer(tim);
    }
    
    public static void closePortal(String time, String portal){
        Timer tim = new Timer();
        Date times = DateHelper.getTime(time, false);
        tim.schedule(new ClosePortal(portal, times, tim), times);
        Teleports.getInstance().addTimer(tim);
    }
    
    public static void extraOpenPortal(String time, String portal, boolean nextTime){
        Timer tim = new Timer();
        Date times = DateHelper.getTime(time.split("-")[0], nextTime);
        tim.schedule(new ExtraClosePortal(portal, time, times, tim), times);
        Teleports.getInstance().addTimer(tim);
    }
    
    public static void extraClosePortal(String time, String portal){
        Timer tim = new Timer();
        Date times = DateHelper.getTime(time.split("-")[1], false);
        tim.schedule(new ExtraOpenPortal(portal, time, times, tim), times);
        Teleports.getInstance().addTimer(tim);
    }
    
    private static class OpenPortal extends TimerTask {
        
        private String name;
        private Date tim;
        private Timer tis;
        
        public OpenPortal(String name, Date time, Timer tim){
            this.name = name;
            this.tim = time;
            tis = tim;
        }

        @Override
        public void run() {
            if (System.currentTimeMillis()-tim.getTime()<1000) {
                Teleports.getPortals().getPortal(name).setOpen(true);
                PortalManager.closePortal(Teleports.getPortals().getToStringTime(name), name);
                Teleports.getInstance().removeTimer(tis);
                tis.cancel();
            }
        }
        
    }
    
    private static class ClosePortal extends TimerTask {

        private String name;
        private Date tim;
        private Timer tis;

        public ClosePortal(String name, Date time, Timer tim) {
            this.name = name;
            this.tim = time;
            tis = tim;
        }

        @Override
        public void run() {
            if (System.currentTimeMillis() - tim.getTime() < 1000) {
                Teleports.getPortals().getPortal(name).setOpen(false);
                PortalManager.openPortal(Teleports.getPortals().getFromStringTime(name), name, true);
                Teleports.getInstance().removeTimer(tis);
                tis.cancel();
            }
        }

    }
    
    private static class ExtraOpenPortal extends TimerTask {
        
        private String name, time;
        private Date tim;
        private Timer tis;
        
        public ExtraOpenPortal(String name, String time, Date date, Timer tim) {
            this.name = name;
            this.time = time;
            this.tim = date;
            tis = tim;
        }
        
        @Override
        public void run(){
            if (System.currentTimeMillis() - tim.getTime() < 1000) {
                Teleports.getPortals().getPortal(name).setExtraOpen(false);
                PortalManager.extraClosePortal(time, name);
                Teleports.getInstance().removeTimer(tis);
                tis.cancel();
            }
        }
        
    }
    
    private static class ExtraClosePortal extends TimerTask {
        
        private String name, time;
        private Date tim;
        private Timer tis;
        
        public ExtraClosePortal(String name, String time, Date date, Timer tim) {
            this.name = name;
            this.time = time;
            this.tim = date;
            tis = tim;
        }
        
        @Override
        public void run(){
            if (System.currentTimeMillis() - tim.getTime() < 1000) {
                Teleports.getPortals().getPortal(name).setExtraOpen(false);
                PortalManager.extraOpenPortal(time, name, true);
                Teleports.getInstance().removeTimer(tis);
                tis.cancel();
            }
        }
        
    }

}