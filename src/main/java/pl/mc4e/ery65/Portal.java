package pl.mc4e.ery65;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

public class Portal implements ConfigurationSerializable {
    
    private Location to,loc1,loc2;
    private List<Integer> x = Lists.newArrayList(),y=Lists.newArrayList(),z=Lists.newArrayList();
    private World world;
    private boolean command = false, open = true, extraOpen = false;
    private List<String> comm = Lists.newArrayList();
    private List<Chunk> chunks = Lists.newArrayList();
    
    public boolean isIn(Location loc){
        if (open){
            if (loc.getWorld().equals(world)){
                if (chunks.contains(loc.getChunk())&&to!=null){
                    if (x.contains(loc.getBlockX())&&y.contains(loc.getBlockY())&&z.contains(loc.getBlockZ()))
                        return true;
                    else
                        return false;
                } else
                    return false;
            } else
                return false;
        } else
            return false;
    }
    
    private void blockPhysics(){
        Location max = new Location(world, Math.max(loc1.getBlockX(), loc2.getBlockX()), 
                Math.max(loc1.getBlockY(), loc2.getBlockY()), Math.max(loc1.getBlockZ(), loc2.getBlockZ()));
        Location min = new Location(world, Math.min(loc1.getBlockX(), loc2.getBlockX()), 
                Math.min(loc1.getBlockY(), loc2.getBlockY()), Math.min(loc1.getBlockZ(), loc2.getBlockZ()));
        for (int x = min.getBlockX(); x <= max.getBlockX(); x++){
            for (int y = min.getBlockY(); y <= max.getBlockY(); y++){
                for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++){
                    Teleports.getInstance().blockPhysics.add(new Location(world, x, y, z));
                }
            }
        }
    }
    
    public Portal(Location loc1, Location loc2, Location to, List<String> commands){
        world = loc1.getWorld();
        this.loc1 = loc1;
        this.loc2 = loc2;
        blockPhysics();
        x = LocationHelper.getIntegerList(loc1.getBlockX(), loc2.getBlockX());
        y = LocationHelper.getIntegerList(loc1.getBlockY(), loc2.getBlockY());
        z = LocationHelper.getIntegerList(loc1.getBlockZ(), loc2.getBlockZ());
        this.to  = to;
        chunks = LocationHelper.getCunks(loc1, loc2);
        blockPhysics();
        if (commands!=null){
            command = true;
            comm = commands;
        }
        Bukkit.getScheduler().runTaskLater(Teleports.getInstance(), new Runnable(){
            
            public void run(){
                setOpen(true);
            }
            
        }, 3L);
    }
    
    public Portal(Location loc1, Location loc2){
        world = loc1.getWorld();
        this.loc2 = loc2;
        this.loc1 = loc1;
        blockPhysics();
        x = LocationHelper.getIntegerList(loc1.getBlockX(), loc2.getBlockX());
        y = LocationHelper.getIntegerList(loc1.getBlockY(), loc2.getBlockY());
        z = LocationHelper.getIntegerList(loc1.getBlockZ(), loc2.getBlockZ());
        chunks = LocationHelper.getCunks(loc1, loc2);
        Bukkit.getScheduler().runTaskLater(Teleports.getInstance(), new Runnable(){
            
            public void run(){
                setOpen(false);
            }
            
        }, 3L);
    }
    
    public Portal(Location loc1, Location loc2, List<String> commands) {
        world = loc1.getWorld();
        this.loc1 = loc1;
        this.loc2 = loc2;
        blockPhysics();
        chunks = LocationHelper.getCunks(loc1, loc2);
        x = LocationHelper.getIntegerList(loc1.getBlockX(), loc2.getBlockX());
        y = LocationHelper.getIntegerList(loc1.getBlockY(), loc2.getBlockY());
        z = LocationHelper.getIntegerList(loc1.getBlockZ(), loc2.getBlockZ());
        if (commands!=null){
            command = true;
            comm = commands;
        }
        Bukkit.getScheduler().runTaskLater(Teleports.getInstance(), new Runnable(){
            
            public void run(){
                setOpen(false);
            }
            
        }, 3L);
    }

    public void runcmds(Player player){
        for (String command: comm){
            if (command.startsWith("/")){
                Bukkit.dispatchCommand(player, command.replace("%player%", player.getName()));
            } else if (command.startsWith("connect")){
                player.sendMessage("§aLaczenie...");
                connect(command, player);
            } else {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("%player%", player.getName()));
            }
        }
    }
    
    private void connect(String server, Player player){
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream d = new DataOutputStream(b);
        
        try {
            d.writeUTF("Connect");
            d.writeUTF(server.split(":")[1]);
        } catch (IOException e) {
            //e.printStackTrace();
        }
        
        player.sendPluginMessage(Teleports.getInstance(), "BungeeCord", b.toByteArray());
    }
    
    public List<Integer> getX(){
        return x;
    }
    
    public List<Integer> getY(){
        return y;
    }
    
    public List<Integer> getZ(){
        return z;
    }
    
    public Location getTo(){
        return to;
    }
    
    public List<String> getCommands(){
        return comm;
    }
    
    public List<Chunk> getChunks(){
        return chunks;
    }
    
    public boolean hasCommands(){
        return command;
    }
    
    public boolean isExtraOpen(){
        return extraOpen;
    }
    
    public void setOpen(boolean toggle){
        if (isExtraOpen())
            return;
        open = toggle;
        open(toggle);
    }
    
    @SuppressWarnings("deprecation")
    private void open(boolean toggle){
        Location max = new Location(world, Math.max(loc1.getBlockX(), loc2.getBlockX()), 
                Math.max(loc1.getBlockY(), loc2.getBlockY()), Math.max(loc1.getBlockZ(), loc2.getBlockZ()));
        Location min = new Location(world, Math.min(loc1.getBlockX(), loc2.getBlockX()), 
                Math.min(loc1.getBlockY(), loc2.getBlockY()), Math.min(loc1.getBlockZ(), loc2.getBlockZ()));
        if (toggle){
            for (int x = min.getBlockX(); x <= max.getBlockX(); x++){
                for (int y = min.getBlockY(); y <= max.getBlockY(); y++){
                    for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++){
                        Block b = world.getBlockAt(x, y, z);
                        if (b.getType() == Material.AIR || b.getType() == Material.STAINED_GLASS){
                            b.setType(Material.STATIONARY_WATER);
                        }
                    }
                }
            }
        } else {
            for (int x = min.getBlockX(); x <= max.getBlockX(); x++){
                for (int y = min.getBlockY(); y <= max.getBlockY(); y++){
                    for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++){
                        Block b = world.getBlockAt(x, y, z);
                        if (b.getType() == Material.AIR || b.getType() == Material.STATIONARY_WATER){
                            b.setType(Material.STAINED_GLASS);
                            b.setData(DyeColor.BLACK.getData());
                        }
                    }
                }
            }
        }
    }
    
    public void setTo(Location loc){
        to = loc;
    }
    
    public void addCmd(String command){
        comm.add(command);
        this.command = true;
    }
    
    public void removeCmd(String command){
        comm.remove(command);
        if (comm == null || comm.isEmpty()){
            if (hasCommands()){
                this.command = false;
            }
        }
    }
    
    public void setExtraOpen(boolean toggle){
        extraOpen = toggle;
        open(toggle);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> o = new HashMap<String, Object>();
        o.put("world", world.getName());
        o.put("loc1", LocationHelper.getLocation(loc1));
        o.put("loc2", LocationHelper.getLocation(loc2));
        if (to!=null){
            o.put("to", LocationHelper.getLocationW(to));
        }
        if (comm != null && !comm.isEmpty()){
            o.put("commands", comm);
        }
        return o;
    }

    @SuppressWarnings("deprecation")
    public void teleport(Player player) {
        player.playEffect(player.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
        player.teleport(to);
        player.playEffect(to, Effect.MOBSPAWNER_FLAMES, 1);
        runcmds(player);
    }

}
