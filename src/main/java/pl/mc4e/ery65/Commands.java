package pl.mc4e.ery65;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        
        if (args.length == 2){
            if (args[1].equalsIgnoreCase("create")){
                if (cs instanceof Player){
                    if (Teleports.getInstance().hasTwoPoint(cs.getName())){
                        if (!Teleports.getPortals().contains(args[0])){
                            Teleports.getPortals().CreateNewPortal(args[0], Teleports.getInstance().getFirstPoint(cs.getName()), Teleports.getInstance().getSeconPoint(cs.getName()));
                            cs.sendMessage("§aPomyslnie stworzyles portal: §6" + args[0]);
                            cs.sendMessage("§2Teraz ustaw gdzie portal ma teleportowac za pomoca:");
                            cs.sendMessage("§b/at §a" + args[0] + " §bsetto");
                            return true;
                        } else {
                            cs.sendMessage("§cTaki portal juz istnieje! §6(§7" + args[0]+ "§6)");
                        }
                    } else {
                        cs.sendMessage("§cNajpierw musisz ustawic dwa punkty za pomoca patyka!");
                        return true;
                    }
                } else {
                    cs.sendMessage("§cKomenda tylko dla gracza!");
                    return true;
                }
            } else if (args[1].equalsIgnoreCase("remove")){
                if (Teleports.getPortals().contains(args[0])){
                    Teleports.getPortals().removePortal(args[0]);
                    cs.sendMessage("§aPomyslnie usunieto portal: §6" + args[0]);
                    return true;
                } else {
                    cs.sendMessage("§cNie znaleziono portalu: §6" + args[0]);
                    return true;
                }
            } else if (args[1].equalsIgnoreCase("open")){
                if (Teleports.getPortals().contains(args[0])){
                    Teleports.getPortals().getPortal(args[0]).setOpen(true);
                    cs.sendMessage("§aPomyslnie otworzyles portal: §6" + args[0]);
                    return true;
                } else {
                    cs.sendMessage("§cNie znaleziono portalu: §6" + args[0]);
                    return true;
                }
            } else if (args[1].equalsIgnoreCase("close")){
                if (Teleports.getPortals().contains(args[0])){
                    Teleports.getPortals().getPortal(args[0]).setOpen(false);
                    cs.sendMessage("§aPomyslnie zamknales portal: §6" + args[0]);
                    return true;
                } else {
                    cs.sendMessage("§cNie znaleziono portalu: §6" + args[0]);
                    return true;
                }
            } else if (args[1].equalsIgnoreCase("setto")){
                if (cs instanceof Player){
                    if (Teleports.getPortals().contains(args[0])){
                        Teleports.getPortals().getPortal(args[0]).setTo(((Player)cs).getLocation());
                        Teleports.getPortals().serializePortal(args[0]);
                        Teleports.SavePortalConfig();
                        Teleports.reloadPortalConfig();
                        cs.sendMessage("§aPomyslnie ustawiles lokalizacje dla portalu: §6" + args[0]);
                        return true;
                    } else {
                        cs.sendMessage("§cNie znaleziono portalu: §6" + args[0]);
                        return true;
                    }
                } else {
                    cs.sendMessage("§cKomenda tylko dla gracza!");
                    return true;
                }
            }
        } if (args.length>2){
            if (args[1].equalsIgnoreCase("addcmd")){
                if (Teleports.getPortals().contains(args[0])){
                    Teleports.getPortals().getPortal(args[0]).addCmd(buildCmd(2," ",args));
                    cs.sendMessage("§aPomyslnie dodales komende (§7" + buildCmd(2," ",args) + "§a) dla portalu: §6"+ args[0]);
                    Teleports.getPortals().serializePortal(args[0]);
                    Teleports.SavePortalConfig();
                    Teleports.reloadPortalConfig();
                    return true;
                } else {
                    cs.sendMessage("§cNie znaleziono portalu: §6" + args[0]);
                    return true;
                }
            } else if (args[1].equalsIgnoreCase("addcmd")){
                if (Teleports.getPortals().contains(args[0])){
                    Teleports.getPortals().getPortal(args[0]).removeCmd(buildCmd(2," ",args));
                    cs.sendMessage("§aPomyslnie usunales komende (§7" + buildCmd(2," ",args) + "§a) dla portalu: §6"+ args[0]);
                    Teleports.getPortals().serializePortal(args[0]);
                    Teleports.SavePortalConfig();
                    Teleports.reloadPortalConfig();
                    return true;
                } else {
                    cs.sendMessage("§cNie znaleziono portalu: §6" + args[0]);
                    return true;
                }
            }
        }
        return false;
    }
    
    private String buildCmd(int start, String spacer, String[] args){
        StringBuilder b = new StringBuilder();
        for (int i = start;i<args.length;i++){
            b.append(args[i] + spacer);
        }
        b.delete(b.toString().length()-spacer.length(), b.toString().length());
        return b.toString();
    }

}
