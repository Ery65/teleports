package pl.mc4e.ery65;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class Listeners implements Listener {
    
    @EventHandler
    void onClick(PlayerInteractEvent e){
        if (e.getPlayer().isOp()){
            if (!e.hasItem()){
                return;
            }
            if (e.getItem().equals(new ItemStack(Material.STICK))){
                if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                    Teleports.getInstance().setSecondPoint(e.getPlayer(), e.getClickedBlock().getLocation());
                    e.getPlayer().sendMessage("§aPomyslnie ustawiles pkt 2");
                } else if (e.getAction().equals(Action.LEFT_CLICK_BLOCK)){
                    Teleports.getInstance().setFirstPoint(e.getPlayer(), e.getClickedBlock().getLocation());
                    e.getPlayer().sendMessage("§aPomyslnie ustawiles pkt 1");
                }
            }
        }
    }
    
    @EventHandler
    void BlockPhysics(BlockPhysicsEvent e){
        Location blockLoc = new Location(e.getBlock().getWorld(), e.getBlock().getLocation().getBlockX(),
                e.getBlock().getLocation().getBlockY(), e.getBlock().getLocation().getBlockZ());
        if (Teleports.getInstance().blockPhysics.contains(blockLoc)){
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    void onBreak(BlockBreakEvent e){
        if (e.getPlayer().isOp()){
            if (e.getPlayer().getGameMode().equals(GameMode.CREATIVE)){
                if (e.getPlayer().getItemInHand().equals(new ItemStack(Material.STICK))){
                    e.setCancelled(true);
                }
            }
        }
    }
    
    @EventHandler
    void onQuit(PlayerQuitEvent e){
        if (e.getPlayer().isOp()){
            Teleports.getInstance().removeFirstPoint(e.getPlayer().getName());
            Teleports.getInstance().removeSecondPoint(e.getPlayer().getName());
        }
    }
    
    @EventHandler(priority = EventPriority.LOW)
    void onMove(PlayerMoveEvent e){
        if (e.getTo()==e.getFrom())
            return;
        if (e.getFrom().distance(e.getTo())<0.10){
            return;
        }
        //System.out.println(Teleports.getPortals().getPortal("test").getChunks());
        if (Teleports.getPortals().isUsedChunk(e.getTo().getChunk())){
            Teleports.getPortals().moveEvent(e.getPlayer(), e.getTo());
        }
    }

}
