package pl.mc4e.ery65;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class CraftPortals {
    
    private List<Chunk> chunks = Lists.newArrayList();
    private Map<String, Portal> portals = Maps.newHashMap();
    private Map<String, String> toTime = Maps.newHashMap();
    private Map<String, String> fromTime = Maps.newHashMap();
    
    
    public CraftPortals(ConfigurationSection s){
        Set<Chunk> x = new HashSet<Chunk>();
        if (s==null)
            return;
        for (String key : s.getKeys(false)){
            World w = Bukkit.getWorld(s.getString(key+".world"));
            int x1,x2,x3,y1,y2,y3,z1,z2,z3;
            List<String> commands = s.getStringList(key+".commands");
            x1 = s.getInt(key+".loc1.x");
            x2 = s.getInt(key+".loc2.x");
            x3 = s.getInt(key+".to.x");
            y1 = s.getInt(key+".loc1.y");
            y2 = s.getInt(key+".loc2.y");
            y3 = s.getInt(key+".to.y");
            z1 = s.getInt(key+".loc1.z");
            z2 = s.getInt(key+".loc2.z");
            z3 = s.getInt(key+".to.z");
            Location loc1 = new Location(w,x1,y1,z1);
            Location loc2 = new Location(w,x2,y2,z2);
            Portal p;
            if (s.getString(key+".to.world")!=null){
                World w2 = Bukkit.getWorld(s.getString(key+".to.world"));
                float yaw = Float.valueOf(s.getString(key+".to.yaw"));
                float pitch = Float.valueOf(s.getString(key+".to.pitch"));
                Location to = new Location(w2,x3,y3,z3,yaw,pitch);
                p = new Portal(loc1,loc2,to,commands);
            } else {
                p = new Portal(loc1,loc2,commands);
            }
            portals.put(key, p);
            x.addAll(p.getChunks());
        }
        chunks.addAll(x);
    }
    
    public CraftPortals(){
    }
    
    public void moveEvent(Player player, Location loc){
        for (Map.Entry<String, Portal> a : portals.entrySet()){
            if (a.getValue().isIn(loc)){
                a.getValue().teleport(player);
                return;
            }
        }
    }
    
    public void addToStringTime(String name, String time){
        toTime.put(name, time);
    }
    
    public void addFromStringTime(String name, String time){
        fromTime.put(name, time);
    }
    
    public String getToStringTime(String portal){
        return toTime.get(portal);
    }
    
    public void removeFromTime(String name){
        fromTime.remove(name);
    }
    
    public void removeToTime(String name){
        toTime.remove(name);
    }
    
    public boolean containsToTime(String portal){
        return toTime.containsKey(portal);
    }
    
    public String getFromStringTime(String portal){
        return fromTime.get(portal);
    }
    
    public boolean containsFromTime(String portal){
        return fromTime.containsKey(portal);
    }
    
    public boolean isUsedChunk(Chunk chunk){
        return chunks.contains(chunk);
    }
    
    public boolean contains(String portal){
        return portals.containsKey(portal);
    }
    
    public Portal getPortal(String name){
        return portals.get(name);
    }
    
    public void CreateNewPortal(String name, Location loc1, Location loc2){
        Portal p = new Portal(loc1, loc2);
        portals.put(name, p);
        Teleports.getPortalConfig().set("Portale." + name, p.serialize());
        Teleports.SavePortalConfig();
        Teleports.reloadPortalConfig();
    }
    
    public void serializePortal(String name){
        Teleports.getPortalConfig().set("Portale." + name, portals.get(name).serialize());
        Teleports.SavePortalConfig();
        Teleports.reloadPortalConfig();
    }
    
    public void removePortal(String name){
        portals.remove(name);
        Teleports.getPortalConfig().set("Portale." + name, null);
    }
    
}
