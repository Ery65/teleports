package pl.mc4e.ery65;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class Teleports extends JavaPlugin {
    
    protected List<Timer> usedTasks = new ArrayList<Timer>();
    private static Teleports instance;
    private static CraftPortals cp;
    private Map<String, Location> firstPoint = Maps.newHashMap();
    private Map<String, Location> secondPoint = Maps.newHashMap();
    public List<Location> blockPhysics = Lists.newArrayList();
    private static FileConfiguration p;
    private static File pp;
    
    public void onEnable(){
        instance = this;
        pp = new File(instance.getDataFolder()+ File.separator + "portals.yml");
        p = YamlConfiguration.loadConfiguration(pp);
        if (p.getConfigurationSection("Portale")!=null){
            cp = new CraftPortals(p.getConfigurationSection("Portale"));
        } else {
            cp = new CraftPortals();
        }
        getCommand("teleports").setExecutor(new Commands());
        getServer().getPluginManager().registerEvents(new Listeners(), this);
        
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        
        Bukkit.getScheduler().runTaskLater(this, new Runnable(){
            
            public void run(){
                check();
            }
            
        }, 15L);
    }
    
    public void onDisable(){
        Iterator<Timer> t = usedTasks.iterator();
        while (t.hasNext()){
            Timer tim = t.next();
            tim.cancel();
            t.remove();
        }
    }
    
    public void check(){
        File a = new File(this.getDataFolder() + File.separator + "Timers.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(a);
        if (!a.exists()){
            getDataFolder().mkdirs();
            try {
                a.createNewFile();
                PrintWriter wr = new PrintWriter(a);
                wr.println("Tasks:");
                wr.println("#  Example:");
                wr.println("#    from: 'everyday,18:00:00'");
                wr.println("#    to: 'everyday,20:00:00'");
                wr.println("Extra:");
                wr.println("#  Example:");
                wr.println("#    times:");
                wr.println("#      - 'saturday,6:00:00-sunday,22:00:00'");
                wr.println("#      - 'monday,6:00:00-monday,12:00:00'");
                wr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ConfigurationSection tasks = cfg.getConfigurationSection("Tasks");
        if (tasks != null){
            for (String key: tasks.getKeys(false)){
                String from = tasks.getString(key + ".from");
                String to = tasks.getString(key + ".to");
                if (cp.contains(key)){
                    if (DateHelper.isOpen(from, to)){
                        PortalManager.closePortal(to, key);
                        cp.addFromStringTime(key, from);
                        cp.addToStringTime(key, to);
                        cp.getPortal(key).setOpen(true);
                    } else {
                        PortalManager.openPortal(from, key, false);
                        cp.addFromStringTime(key, from);
                        cp.addToStringTime(key, to);
                        cp.getPortal(key).setOpen(false);
                    }
                }
            }
        }
        ConfigurationSection extra = cfg.getConfigurationSection("Extra");
        if (extra != null){
            for (String key : extra.getKeys(false)){
                List<String> times = extra.getStringList(key + ".times");
                if (times != null) {
                    if (cp.contains(key)){
                        for (String time : times){
                            String[] splitTime = time.split("-");
                            if (DateHelper.isOpen(splitTime[0], splitTime[1])){
                                PortalManager.extraClosePortal(time, key);
                                cp.getPortal(key).setExtraOpen(true);
                            } else {
                                PortalManager.extraOpenPortal(time, key, false);
                                cp.getPortal(key).setExtraOpen(false);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public boolean hasTwoPoint(String player){
        return (firstPoint.containsKey(player)&&secondPoint.containsKey(player));
    }
    
    public Location getFirstPoint(String name){
        return firstPoint.get(name);
    }
    
    public Location getSeconPoint(String name){
        return secondPoint.get(name);
    }
    
    public static Teleports getInstance(){
        return instance;
    }
    
    public void addTimer(Timer tim){
        usedTasks.add(tim);
    }
    
    public void removeTimer(Timer tim){
        usedTasks.remove(tim);
        if (usedTasks == null){
            usedTasks = new ArrayList<Timer>();
        }
    }
    
    public void setFirstPoint(Player p, Location loc){
        firstPoint.put(p.getName(), loc);
    }
    
    public void setSecondPoint(Player p, Location loc){
        secondPoint.put(p.getName(), loc);
    }
    
    public void removeFirstPoint(String name){
        firstPoint.remove(name);
        if (firstPoint == null){
            firstPoint = Maps.newHashMap();
        }
    }
    
    public void removeSecondPoint(String name){
        secondPoint.remove(name);
        if (secondPoint == null){
            secondPoint = Maps.newHashMap();
        }
    }
    
    public static FileConfiguration getPortalConfig(){
        return p;
    }
    
    public static void SavePortalConfig(){
        try {
            p.save(pp);
        } catch (IOException e){
            
        }
        reloadPortalConfig();
    }
    
    public static void reloadPortalConfig(){
        pp = new File(instance.getDataFolder()+ File.separator + "portals.yml");
        p = YamlConfiguration.loadConfiguration(pp);
        cp = new CraftPortals(p.getConfigurationSection("Portale"));
        instance.check();
    }
    
    public static CraftPortals getPortals(){
        return cp;
    }

}
